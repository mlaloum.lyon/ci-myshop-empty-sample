package com.ci.myShop.model;

public class Item {
	private String name;
	private int id;
	private float price;
	private int nbrElt;
	
	
	public String display() {
		String result="name+id+price+nbrElt";
		return result;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public float getPrice() {
		return price;
	}


	public void setPrice(float price) {
		this.price = price;
	}


	public int getNbrElt() {
		return nbrElt;
	}


	public void setNbrElt(int nbrElt) {
		this.nbrElt = nbrElt;
	}
}