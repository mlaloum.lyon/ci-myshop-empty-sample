package com.ci.myShop.controller;

import com.ci.myShop.model.Item;

public class Shop {
	private Storage storage;
	private float cash;
	
	public Item sell(String name) {
		if (this.storage.getItem(name) != null) {
			Item item = this.storage.getItem(name);
			this.setCash(this.getCash() + item.getPrice());
			return item;
		}
		else {
			return null;
		}
	}

	public float getCash() {
		return cash;
	}

	public void setCash(float cash) {
		this.cash = cash;
	}
} 
