package com.ci.myShop.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class ItemTest {
	@Test
	public void createItemTest () {
		Item item = new Item("name", 0, 100, 2f, 10);
		assertEquals(item.getName(), "name");
	}
}